# Venv template

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/venv/badges/main/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/venv/-/pipelines)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://lab.frogg.it/swepy/cicd-templates/venv/-/blob/main/LICENSE)

## Objective

The objective of the `venv` job is to provide a Python environment with all
requirements installed for CI jobs. This reusable job can help speed up other jobs
creation and ensure consistent configuration across CI jobs that rely on a python
environment.

## How to use it

1. Include the `venv` template in your CI/CD configuration (see quick use above).
2. If you need to customize the job, check
   the [jobs customization](https://docs.r2devops.io/get-started/use-templates/#job-templates-customization).

## Variables

| Name                     | Description                                                                                                     | Default                                    |
|--------------------------|-----------------------------------------------------------------------------------------------------------------|--------------------------------------------|
| `IMAGE_NAME`             | The default name for the docker image.                                                                          | `"python"`                                 |
| `IMAGE_TAG`              | The default tag for the docker image.                                                                           | `"latest"`                                 |
| `IMAGE`                  | The default docker image name.                                                                                  | `"$IMAGE_NAME:$IMAGE_TAG"`                 |
| `PROJECT_PATH`           | The path to the project root directory.                                                                         | `"."`                                      |
| `REQUIREMENTS_FILE_PATH` | the path to the requirements file.                                                                              | `"$PROJECT_PATH/requirements.txt"`         |
| `PYTHON_SETUP`           | Bash commands to setup your python environment. Default rely on `requirements.txt` to install all dependencies. | `"pip install -r $REQUIREMENTS_FILE_PATH"` |
| `VENV_NAME`              | The name of the virtual environment.                                                                            | `"venv"`                                   |
| `VENV_ARTIFACT_NAME`     | The name of the virtual environment artifact.                                                                   | `"$VENV_NAME"`                             |
 
See [Python Docker Official Image](https://hub.docker.com/_/python) for supported tags and respective Dockerfile links.

## Example of use

WIP
