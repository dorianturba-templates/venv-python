# Changelog

All notable changes to this job will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2024-04-06

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/venv/badges/venv@1.0.0/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/venv/-/pipelines?ref=venv%401.0.0)

### Added

* MIT license attached to the template.
* Badges in README.md.
* Debug "echo" in the job.

### Changed

* Pipelines badges to the CHANGELOG.md file for each version.
* Template CICD validation include. It now includes the job from the current host and
  project path to handle forks.

### Removed

* Unnecessary configuration files.

## [0.1.0] - 2024-04-05

[![Pipeline](https://lab.frogg.it/swepy/cicd-templates/venv/badges/venv@0.1.0/pipeline.svg)](https://lab.frogg.it/swepy/cicd-templates/venv/-/pipelines?ref=venv%400.1.0)

* Initial version
